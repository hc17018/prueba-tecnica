 var pacientes_json=[
    {

        "id_paciente":1,
        "nombre_paciente": "Emily Hernández",
        "dui_paciente": "01020304-5",
        "edad_paciente": "19",
        "telefono_paciente": "22771579"

    },
    {

        "id_paciente":2,
        "nombre_paciente": "Abigail Vasquez",
        "dui_paciente": "01020304-6",
        "edad_paciente": "21", 
        "telefono_paciente": "22575777"

    },
    {

        "id_paciente":3,
        "nombre_paciente": "Amilcar Chavez",
        "dui_paciente": "01598746-5",
        "edad_paciente": "24",
        "telefono_paciente": "77898745"

    },
    {

        "id_paciente":4,
        "nombre_paciente": "Nancy Chavez",
        "dui_paciente": "01020304-5",
        "edad_paciente": "35",
        "telefono_paciente": "22771579"

    },
    {

        "id_paciente":5,
        "nombre_paciente": "Ricardo Reionsa",
        "dui_paciente": "05879468-8",
        "edad_paciente": "27",
        "telefono_paciente": "7778888"

    },
    {

        "id_paciente":6,
        "nombre_paciente": "Salvador Maldona",
        "dui_paciente": "03569879-8",
        "edad_paciente": "29",
        "telefono_paciente": "78787979"

    },
    {

        "id_paciente":7,
        "nombre_paciente": "Domi Garcia",
        "dui_paciente": "01000000-1",
        "edad_paciente": "38",
        "telefono_paciente": "72727373"

    },
    {

        "id_paciente":8,
        "nombre_paciente": "Jazmin Avelar",
        "dui_paciente": "05698749-8",
        "edad_paciente": "54",
        "telefono_paciente": "76767474"

    },
    {

        "id_paciente":9,
        "nombre_paciente": "Ernestina Perdomo",
        "dui_paciente": "01032634-5",
        "edad_paciente": "38",
        "telefono_paciente": "75757171"

    },
    {

        "id_paciente":10,
        "nombre_paciente": "Ricardo Chavez",
        "dui_paciente": "05618688-8",
        "edad_paciente": "18",
        "telefono_paciente": "77016580"

    },
    
    
]