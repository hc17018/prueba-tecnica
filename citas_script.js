$(document).ready(function()
{
    let diagnosticos=[];
    let citas =[]
    if(localStorage.getItem("diagnosticos") != undefined && localStorage.getItem("diagnosticos") != null)
        {
             diagnosticos = localStorage.getItem("diagnosticos");
             diagnosticos = JSON.parse(diagnosticos);
             console.log("diag docm: ", diagnosticos);
            
            // $("#tablaDiagnosticos").html(diagnosticos)

        }
    
    // if(localStorage.getItem("citas") != undefined && localStorage.getItem("citas") != null)
    //     {
    //          citas = localStorage.getItem("citas");
    //         $("#tablaCitas").html(citas)

    //     }
    //     else
    //     {
        
        let filas = []
        console.log("citas_josn: ", citas_json);
        
        citas_json.map(iterador=>{
            let {
                
                id_cita,
                fecha_cita,
                hora_cita,
                estado_cita, 
               
            }=iterador

            let atendido = false;

            let encontrados =[]

            for(let iterador of diagnosticos)
            {
                console.log("it: ",iterador)
                console.log("id_cita: ",id_cita);

                if(iterador.id_cita == id_cita)
                {
                    atendido = true;
                }
            }

            if(atendido==true)
            {
           

                estado_cita ="Atendido";
            }


    
            
            if(estado_cita=="Atendiendo"){
                estado_cita="Atendiendo"
                            {
                            let fila = "<tr id = '"+id_cita + "'>" +
                                        "<td>" + fecha_cita + "</td>" + 
                                        "<td>" + hora_cita + "</td>" +
                                        "<td>" + estado_cita + "</td>" + 
                                        "<td>" + "<button title = 'Agregar descripciones del diagnostico' type='button' id ='"+ id_cita + "aggDiagBtn' class= 'btn btn-sm w-sm btn-success aggDiagBtn'><i class='far fa-list-alt'></i></button>" + "</td>"
                                         filas.push(fila);    
                            }
                        }
            else if (estado_cita=="Atendido")
            {
                    estado_cita="Atendido"
                            let fila = "<tr id = '"+id_cita + "'>" +
                                        "<td>" + fecha_cita + "</td>" + 
                                        "<td>" + hora_cita + "</td>" +
                                        "<td>" + estado_cita + "</td>" +
                                        "<td>" + "<button title ='Ver descripciones del diagnostico' type='button' id ='"+ id_cita + "verDiagBtn' class= 'btn btn-sm w-sm btn-success verDiagBtn'><i class='far fa-eye'></i></button>" + "</td>" 
                                         filas.push(fila);    
          }  
          localStorage.setItem("citas", filas);
          $("#tablaCitas").html(filas)

        })

        localStorage.setItem("citas", filas);

        $("#tablaCitas").html(filas)
        // }

        var pacientesHIsto = localStorage.getItem("pacienteHistorial")
        var expedienteHisto = localStorage.getItem("expedienteHistorial")
        
        $("#nombrePaciente").html(pacientesHIsto)
        $("#codExpediente").html(expedienteHisto)

        /* Boton agregar diagnostico */
        $("body").on("click", ".aggDiagBtn", function(){
            fila = $(this).closest("tr");
            idCita= + $(this).attr("id").split("aggDiagBtn")[0];
            idBtnDiag = "Cita: " + $(this).attr("id").split("aggDiagBtn")[0];
            fechaDiag ="Fecha: " + fila.find('td:eq(0)').text();
            
            
            


            $(location).attr('href', './agregarDiagnostico.html'); 
            localStorage.setItem("idBtnDiag",idBtnDiag) 
            localStorage.setItem("fechaDiag",fechaDiag)  
            localStorage.setItem("idCita", idCita)
        })
        /* Guardando variables para usarlas en agregarDiagnostico.html */
        var idDiag = localStorage.getItem("idBtnDiag")
        var fechaDiag = localStorage.getItem("fechaDiag")
        var idCita = localStorage.getItem("idCita")
        $("#numCita").html(idDiag)
        $("#fechaCita").html(fechaDiag)

        /* Boton para agregar diagnostico a la lista */
        $("body").on("click", ".aggDiagnostico", function(){
            contador = 0;
            diagnostico= $("#ingresoDiagnostico").val();
            $("#tablaDiagnosticos tr").each(function(){
                contador++;
            })
            contador = contador + 1;
            $("#tablaDiagnosticos").append(
                    "<tr id = '"+contador + "'>"+
                        "<td>" + diagnostico + "</td>"+
                            "<td>" + "<button type='button' id ='"+ contador + "eliminarDiagnos' class='btn btn-sm w-sm btn-danger eliminarDiagnos'>Eliminar diagnostico</button>" + "</td>"+ 
                    "</'tr'>"
            )
            /* console.log(contador) */
            /* localStorage.setItem("tablaDiagnosticos", tablaDiagnosticos); */
        })
        /* Boton eliminar diagnostico */
        $("body").on("click", ".eliminarDiagnos", function(){
            let id = $(this).attr("id").split("eliminarDiagnos")[0];
            $("#" + id).remove();
        }) 

        /* Regresar al index */
        $("body").on("click", ".regresarIndexBtn", function(){
            $(location).attr('href', './index.html')
        }) 

        $("body").on("click", ".regresarHistoBtn", function(){
            $(location).attr('href', './citas.html')
        }) 

        /* Boton eliminar todos los diagnosticos */
        $("body").on("click", ".limpiarBtn", function(){
            $("#tablaDiagnosticos tr").each(function(){
                let id = $(this).attr("id").split("eliminarDiagnos")[0];
                $("#" + id).remove();
            })
            
        })
        /* Boton guardar diagnosticos */
        $("body").on("click", ".guardarBtn", function(){
            let diagnosticos = []
            $("body #modalGuardarBtn").modal('show');
            
                $("#siBtn").on("click", function(){
                    console.log("entre")
                    $("#tablaDiagnosticos tr").each(function(){    
                
                        idFila = $(this).attr("id");
                        diagnosticoFila = $(this).find('td:eq(0)').text();
                        // console.log(idFila)
                        // console.log(diagnosticoFila)
        
                        // var diagnostico=[
                        //     idFilas = "<tr id = '"+ idFila + "'>",
                        //     diagnosticoFilas = "<td>" + diagnosticoFila + "</td>",
                        //     eliminarBtn = "<td>" + "<button type='button' id ='"+ idFila + "eliminarDiagnos' class='btn btn-sm w-sm btn-danger eliminarDiagnos'>Eliminar diagnostico</button>" + "</td>"
                        // ]

                        let diagnostico_obj={
                            id_diagnostico: idFila,
                            diagnostico: diagnosticoFila
                        }
                        console.log(diagnostico)
                        diagnosticos.push(diagnostico_obj)
                        console.log(diagnosticos)
                    })
                    var idCita = localStorage.getItem("idCita")

                    let n_diagnostico ={
                        id_cita: idCita,
                        diagnosticos
                    } 
                    let diagnosticos_citas =[];

                    let obtenido_storage = localStorage.getItem("diagnosticos");
                    obtenido_storage = JSON.parse(obtenido_storage);
                    if(obtenido_storage != undefined && obtenido_storage != null)
                    {
                        diagnosticos_citas = obtenido_storage;

                        console.log("diagnosticos_citas: ", diagnosticos_citas);
                        diagnosticos_citas.push(n_diagnostico);
                    }
                    else{
                        diagnosticos_citas.push(n_diagnostico)
                    }
                    
                    localStorage.setItem("diagnosticos",  JSON.stringify(diagnosticos_citas))
                    console.log("el localStorage; ,", localStorage.getItem("diagnosticos"));
                    $("body #modalGuardarBtn").modal('hide');
                    $(location).attr('href', './citas.html');
                   

                    // console.log(citas_json.each(function(){
                    //     console.log()
                    // }))
                })  
                
        })
        $("body").on("click", ".verDiagBtn", function(){
            let id = $(this).attr("id").split("verDiagBtn")[0];
            fila = $(this).closest("tr");
            fechaAten = fila.find('td:eq(0)').text();
    
            console.log(id , fechaAten);
            localStorage.setItem("idCitaAte", id );
            localStorage.setItem("fechaAten", fechaAten );
        
            $(location).attr('href', './descripcionesDiagnostico.html');

            console.log(texto)
        })
        var texto = ""
            texto+=localStorage.getItem("fechaAten");
            texto+="\n"
            idCitaAte=localStorage.getItem("idCitaAte")
            for(let iterador of diagnosticos)
            {
                console.log("it: ",iterador)
                console.log("idCitaAte: ",idCitaAte);

                if(iterador.id_cita == idCitaAte)
                {
                   for(let it of iterador.diagnosticos)
                   {
                        texto+=it.diagnostico;
                        texto+="\n"
                   }

                  
                }
            }
            $("#textAreaDiagnostico").html(texto)

            contador = 0;
            var textoDiagnostico = ""
            for(let iterador of diagnosticos)
            {
               

                if(iterador.id_cita >= contador)
                {
                   for(let it of iterador.diagnosticos)
                   {
                    
                    textoDiagnostico+=it.diagnostico;
                    textoDiagnostico+="\n"
                    textoDiagnostico+="\n"
                   }
                }contador++
            }
            console.log(textoDiagnostico)
            $("#textAreaDiagnosticoRealizados").html(textoDiagnostico) 
})