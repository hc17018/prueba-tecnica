// const pacientes_json = require("./pacientes.js");
     $(document).ready(function(){

        /* console.log(pacientes_json) */

        if(localStorage.getItem("pacientes") != undefined && localStorage.getItem("pacientes") != null)
        {
            let pacientes = localStorage.getItem("pacientes");
            $("#tablaPacientes").html(pacientes)

        }
        else
        {
        
        let filas = []
        var fila;
        pacientes_json.map(iterador=>{
            let {
                id_paciente,
                nombre_paciente,
                edad_paciente,
                dui_paciente, 
                telefono_paciente
            }=iterador 
            let fila = "<tr id = '"+id_paciente + "'>" +
                            "<td>" + nombre_paciente + "</td>" + 
                            "<td>" + edad_paciente + "</td>" +
                            "<td>" + dui_paciente + "</td>" +
                            "<td>" + telefono_paciente + "</td>" + 
                            "<td>" + "<button title = 'Editar'type='button' id ='"+ id_paciente + "editarBtn' class= 'btn btn-sm w-sm btn-success editarBtn'><i class='far fa-edit'></i></button> " +
                            "<button title = 'Eliminar paciente' type='button' id ='"+ id_paciente + "eliminarBtn' class='btn btn-sm w-sm btn-danger eliminarBtn'><i class='far fa-trash-alt'></i></button>" +
                            "<button title= 'Ver historial de citas' type='button' id ='"+ id_paciente + "verHistoBtn' class='btn btn-sm w-sm btn-warning verHistoBtn'><i class='far fa-eye'></i></button>" +
                            "<button title='Crear historial clinico' type='button' id ='"+ id_paciente + "crearHistoBtn' class='btn btn-sm w-sm btn-primary crearHistoBtn'><i class='fas fa-stethoscope'></i></button>" + "</td> </tr>"
           filas.push(fila);         
                  
        })
       

        localStorage.setItem("pacientes", filas);

        $("#tablaPacientes").html(filas)
        }

            
        /* Boton editar */
        $("body").on("click",".editarBtn", function(){
            $("body #modalEditar").modal('show');

                idBtn = $(this).attr("id").split("editarBtn")[0];
                fila = $(this).closest("tr");
                nombre = fila.find('td:eq(0)').text();
                edad = parseInt(fila.find('td:eq(1)').text());
                dui = fila.find('td:eq(2)').text();
                telefono = fila.find('td:eq(3)').text();

                $("#nombre_paciente").val(nombre);
                $("#edad_paciente").val(edad);
                $("#dui_paciente").val(dui);
                $("#telefono_paciente").val(telefono);

            $("#actualizarBtn").on("click", function(){

                nombre    =$("#nombre_paciente").val();
                edad       =$("#edad_paciente").val();
                dui        =$("#dui_paciente").val();
                telefono   =$("#telefono_paciente").val();
        
                let filas = []
                    $("#tablaPacientes tr").each(function()
                    {
                        id_paciente = $(this).attr("id");
                        nombre_paciente = $(this).find('td:eq(0)').text();
                        edad_paciente = parseInt($(this).find('td:eq(1)').text());
                        dui_paciente = $(this).find('td:eq(2)').text();
                        telefono_paciente = $(this).find('td:eq(3)').text();

                       if(id_paciente == idBtn)
                       {
                            nombre_paciente = nombre;
                            edad_paciente =edad;
                            dui_paciente = dui;
                            telefono_paciente = telefono;

                        let fila = "<tr id = '"+id_paciente + "'>" +
                            "<td>" + nombre_paciente + "</td>" + 
                            "<td>" + edad_paciente + "</td>" +
                            "<td>" + dui_paciente + "</td>" +
                            "<td>" + telefono_paciente + "</td>" + 
                            "<td>" + "<button type='button' id ='"+ id_paciente + "editarBtn' class= 'btn btn-sm w-sm btn-success editarBtn'>Editar</button> " +
                            "<button type='button' id ='"+ id_paciente + "eliminarBtn' class='btn btn-sm w-sm btn-danger eliminarBtn'>Eliminar paciente</button>" +
                            "<button type='button' id ='"+ id_paciente + "verHistoBtn' class='btn btn-sm w-sm btn-warning verHistoBtn'>Ver historial de citas</button>" +
                            "<button type='button' id ='"+ id_paciente + "crearHistoBtn' class='btn btn-sm w-sm btn-primary crearHistoBtn'>Crear historial clinico</button>" + "</td> </tr>"
                        filas.push(fila);                        
                       }
                       else
                       {
                        let fila = "<tr id = '"+id_paciente + "'>" +
                        "<td>" + nombre_paciente + "</td>" + 
                        "<td>" + edad_paciente + "</td>" +
                        "<td>" + dui_paciente + "</td>" +
                        "<td>" + telefono_paciente + "</td>" + 
                        "<td>" + "<button type='button' id ='"+ id_paciente + "editarBtn' class= 'btn btn-sm w-sm btn-success editarBtn'>Editar</button> " +
                        "<button type='button' id ='"+ id_paciente + "eliminarBtn' class='btn btn-sm w-sm btn-danger eliminarBtn'>Eliminar paciente</button>" +
                        "<button type='button' id ='"+ id_paciente + "verHistoBtn' class='btn btn-sm w-sm btn-warning verHistoBtn'>Ver historial de citas</button>" +
                        "<button type='button' id ='"+ id_paciente + "crearHistoBtn' class='btn btn-sm w-sm btn-primary crearHistoBtn'>Crear historial clinico</button>" + "</td> </tr>"
                    filas.push(fila);
                       }                        
                    }) 
                    $("#tablaPacientes").html(filas)
                    localStorage.setItem("pacientes", filas);
            })
          });
        
        /* Boton eliminar */
        $("body").on("click", ".eliminarBtn", function(){
        let id = $(this).attr("id").split("eliminarBtn")[0];
        
            $("body #modalEliminar").modal('show');

                $("#aceptarEliminar").on("click", function(){ 
                    $("#" + id).remove();
                    $("body #modalEliminar").modal('hide');
                    
                    let filas = []
                    $("#tablaPacientes tr").each(function()
                    {
                        id_paciente = $(this).attr("id");
                        nombre_paciente = $(this).find('td:eq(0)').text();
                        edad_paciente = parseInt($(this).find('td:eq(1)').text());
                        dui_paciente = $(this).find('td:eq(2)').text();
                        telefono_paciente = $(this).find('td:eq(3)').text();
                        

                        let fila = "<tr id = '"+id_paciente + "'>" +
                            "<td>" + nombre_paciente + "</td>" + 
                            "<td>" + edad_paciente + "</td>" +
                            "<td>" + dui_paciente + "</td>" +
                            "<td>" + telefono_paciente + "</td>" + 
                            "<td>" + "<button type='button' id ='"+ id_paciente + "editarBtn' class= 'btn btn-sm w-sm btn-success editarBtn'>Editar</button> " +
                            "<button type='button' id ='"+ id_paciente + "eliminarBtn' class='btn btn-sm w-sm btn-danger eliminarBtn'>Eliminar paciente</button>" +
                            "<button type='button' id ='"+ id_paciente + "verHistoBtn' class='btn btn-sm w-sm btn-warning verHistoBtn'>Ver historial de citas</button>" +
                            "<button type='button' id ='"+ id_paciente + "crearHistoBtn' class='btn btn-sm w-sm btn-primary crearHistoBtn'>Crear historial clinico</button>" + "</td> </tr>"
                        filas.push(fila);   
                    }) 
                    localStorage.setItem("pacientes", filas);
                });       
        });
        
        /* Boton historial de citas */
        $("body").on("click",".verHistoBtn", function(){

                idBtn = $(this).attr("id").split("verHistoBtn")[0]
                fila = $(this).closest("tr");
                nombre = fila.find('td:eq(0)').text();
                expedienteHistorial ="Expediente: "  +
                idBtn + (nombre.split("")[0]) + (nombre.split("")[1])
                pacienteHistorial = "Paciente: " + nombre

                $(location).attr('href', './citas.html');          
                localStorage.setItem("idBtn", idBtn)
                localStorage.setItem("pacienteHistorial", pacienteHistorial);
                localStorage.setItem("expedienteHistorial", expedienteHistorial);
                 
          });  
          /* Boton historial clinico */
        $("body").on("click",".crearHistoBtn", function(){

            console.log("entre")
            idBtn = $(this).attr("id").split("crearHistoBtn")[0]
            fila = $(this).closest("tr");
            nombre = fila.find('td:eq(0)').text();
            expedienteHistorial ="Expediente: "  +
            idBtn + (nombre.split("")[0]) + (nombre.split("")[1])
            pacienteHistorial = "Paciente: " + nombre         
            localStorage.setItem("idBtn", idBtn)
            localStorage.setItem("pacienteHistorial", pacienteHistorial);
            localStorage.setItem("expedienteHistorial", expedienteHistorial);
            $(location).attr('href', './diagnosticosRealizados.html'); 
            
            
      });

      })
